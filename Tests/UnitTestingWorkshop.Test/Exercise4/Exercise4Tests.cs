﻿using Moq;
using NUnit.Framework;
using Umbraco.Core.Models;
using Umbraco.Core.Services;
using UnitTestingWorkshop.BusinessLogic.Services;

namespace UnitTestingWorkshop.Test.Exercise4
{
    [TestFixture]
    public class Exercise4Tests
    {
        IPostService _postService;
        private Mock<IContentService> _contentServiceMock;


        [SetUp]
        public void Setup()
        {
            _contentServiceMock = new Mock<IContentService>();
            _postService = new PostService(_contentServiceMock.Object);
        }

    }
}
